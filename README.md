# python pipeline

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/buildgarden/pipelines/python?branch=main)](https://gitlab.com/buildgarden/pipelines/python/-/commits/main)
[![coverage report](https://img.shields.io/gitlab/pipeline-coverage/buildgarden/pipelines/python?branch=main)](https://gitlab.com/buildgarden/pipelines/python/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/buildgarden/pipelines/python)](https://gitlab.com/buildgarden/pipelines/python/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![cici-tools enabled](https://img.shields.io/badge/%E2%9A%A1_cici--tools-enabled-c0ff33)](https://gitlab.com/buildgarden/tools/cici-tools)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Build, test, and release [Python](https://www.python.org/) packages.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

- [Project setup](#project-setup)

- [Use cases](#use-cases)

- [Targets](#targets)

- [Variables](#variables)

## Project setup

At a minimum, this pipeline requires that:

- A `pyproject.toml` is created

- A package directory is created; `PYTHON_PACKAGE` is defined

## Use cases

### All includes

Add the following to your `.gitlab-ci.yml` file to enable all targets:

```yaml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-autoflake.yml
      - python-black.yml
      - python-build-wheel.yml
      - python-build-sdist.yml
      - python-deptry.yml
      - python-docformatter.yml
      - python-import-linter.yml
      - python-isort.yml
      - python-mypy.yml
      - python-pyroma.yml
      - python-pytest.yml
      - python-twine-upload.yml
      - python-vulture.yml
```

### All hooks

Add the following to your `.pre-commit-config.yaml` file to enable all targets:

```yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/python
    hooks:
      - id: python-autoflake
      - id: python-black
      - id: python-deptry
      - id: python-docformatter
      - id: python-import-linter
      - id: python-isort
      - id: python-pip-compile
      - id: python-pyroma
      - id: python-vulture
```

### Static analysis tools

The following linters are available for use:

```yaml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-autoflake.yml
      - python-black.yml
      - python-isort.yml
      - python-mypy.yml
      - python-pyroma.yml
      - python-vulture.yml
```

### Run unit tests

Run pytest unit tests:

```yaml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-pytest.yml
```

### Customize the behavior of `autoflake`

The following settings are the default options. Paste into your CI file to
customize:

```yaml
variables:
  PYTHON_AUTOFLAKE_OPTS: >-
    --remove-duplicate-keys
    --remove-unused-variables
    --expand-star-imports
    --remove-all-unused-imports
```

### Use `black` and `isort` together

The following minimal configuration is required in your `pyproject.toml` file:

```toml
[tool.isort]
profile = "black"
```

`known_first_party` is also recommended to use the linter without a complete
local environment:

```toml
[tool.isort]
known_first_party = ["python_pipeline"]
profile = "black"
```

### Build and publish

Build a package with setuptools and publish the package to a PyPI repository.

```yaml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-build-sdist.yml
      - python-build-wheel.yml
      - python-twine-upload.yml
```

### Publish the package

By default, this pipeline publishes to GitLab's PyPI Package Registry.

The pipeline can be used to publish to PyPI by setting the following variables:

- `PYTHON_PYPI_UPLOAD_URL` = `https://upload.pypi.org/legacy/`
- `PYTHON_PYPI_USERNAME` = `__token__`
- `PYTHON_PYPI_PASSWORD` - Your API token

### Using Packages

#### Locally

Create the config directory:

```
mkdir -p ~/.config/pip
```

Create a `pip.conf` file:

```
$ cat ~/.config/pip/pip.conf
[global]
index-url = https://__token__:<read_api-token>gitlab.com/api/v4/groups/<group-id>/-/packages/pypi/simple
```

Install a package. GitLab will pull from [pypi.org](https://pypi.org) if the
package is not found.

```
$ pip install python-decouple
Looking in indexes: https://__token__:****@gitlab.com/api/v4/projects/32517826/packages/pypi/simple
Collecting python-decouple
  Downloading python_decouple-3.5-py3-none-any.whl (9.6 kB)
Installing collected packages: python-decouple
Successfully installed python-decouple-3.5
```

#### In CI

The most sensible way to use packages from the GitLab Package Registry is to
pull them from the root namespace registry. This ensures that any packages your
user may have access to are available for use.

The `CI_JOB_TOKEN` cannot be used to determine the group ID by name, so it must
be provided upfront. This is preferred to using a deploy token, so that the
permissions are specific to your user and not blanket.

Define the `PYTHON_PYPI_GITLAB_GROUP_ID` variable in your CI file, or ask a
group owner to add it to CI variables.

## Targets

| Name  | [GitLab include](https://docs.gitlab.com/ee/ci/yaml/includes.html) | [pre-commit hook](https://pre-commit.com/) | Description |
| ----- | ------- | ---- | ----------- |
| [python-autoflake](#python-autoflake) | ✓ | ✓ | Remove unused imports and variables from Python code with [autoflake](https://github.com/PyCQA/autoflake). |
| [python-black](#python-black) | ✓ | ✓ | Format Python code with [Black](https://black.readthedocs.io/en/stable/index.html). |
| [python-build-sdist](#python-build-sdist) | ✓ |  | Create a Python source distribution with [build](https://build.pypa.io/en/stable/). |
| [python-build-wheel](#python-build-wheel) | ✓ |  | Create a Python [wheel](https://pythonwheels.com/) with [build](https://build.pypa.io/en/stable/). |
| [python-deptry](#python-deptry) | ✓ | ✓ | Check for issues with Python dependencies with [deptry](https://github.com/fpgmaas/deptry). |
| [python-docformatter](#python-docformatter) | ✓ | ✓ | Format Python docstrings according to [PEP 257](https://peps.python.org/pep-0257/) with [docformatter](https://github.com/PyCQA/docformatter). |
| [python-import-linter](#python-import-linter) | ✓ | ✓ | Define and enforce Python import rules with [import-linter](https://github.com/seddonym/import-linter). |
| [python-isort](#python-isort) | ✓ | ✓ | Sort Python module imports with [isort](https://github.com/PyCQA/isort). |
| [python-mypy](#python-mypy) | ✓ |  | Perform static type checking with [Mypy](https://github.com/python/mypy). |
| [python-pip-compile](#python-pip-compile) |  | ✓ | Lock Python environments with [uv pip compile](https://docs.astral.sh/uv/pip/compile/). |
| [python-pyroma](#python-pyroma) | ✓ | ✓ | Rate Python package readiness with [pyroma](https://github.com/regebro/pyroma). |
| [python-pytest](#python-pytest) | ✓ | ✓ | Run unit tests with [pytest](https://docs.pytest.org/en/stable/). |
| [python-twine-upload-pypi-oidc](#python-twine-upload-pypi-oidc) | ✓ |  | Publish a Python package to PyPI with [twine](https://github.com/pypa/twine) using [OIDC](https://docs.pypi.org/trusted-publishers/). |
| [python-twine-upload](#python-twine-upload) | ✓ |  | Publish a Python package to PyPI with [twine](https://github.com/pypa/twine) using an API token. |
| [python-vulture](#python-vulture) | ✓ | ✓ | Find dead Python code with [vulture](https://github.com/jendrikseipp/vulture). |

### `python-autoflake`

Remove unused imports and variables from Python code with [autoflake](https://github.com/PyCQA/autoflake).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-autoflake.yml
```

As a pre-commit hook:

```yaml
# .pre-commit-config.yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/python
    rev: ""
    hooks:
      - id: python-autoflake
```

### `python-black`

Format Python code with [Black](https://black.readthedocs.io/en/stable/index.html).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-black.yml
```

As a pre-commit hook:

```yaml
# .pre-commit-config.yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/python
    rev: ""
    hooks:
      - id: python-black
```

### `python-build-sdist`

Create a Python source distribution with [build](https://build.pypa.io/en/stable/).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-build-sdist.yml
```

### `python-build-wheel`

Create a Python [wheel](https://pythonwheels.com/) with [build](https://build.pypa.io/en/stable/).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-build-wheel.yml
```

### `python-deptry`

Check for issues with Python dependencies with [deptry](https://github.com/fpgmaas/deptry).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-deptry.yml
```

As a pre-commit hook:

```yaml
# .pre-commit-config.yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/python
    rev: ""
    hooks:
      - id: python-deptry
```

### `python-docformatter`

Format Python docstrings according to [PEP 257](https://peps.python.org/pep-0257/) with [docformatter](https://github.com/PyCQA/docformatter).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-docformatter.yml
```

As a pre-commit hook:

```yaml
# .pre-commit-config.yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/python
    rev: ""
    hooks:
      - id: python-docformatter
```

### `python-import-linter`

Define and enforce Python import rules with [import-linter](https://github.com/seddonym/import-linter).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-import-linter.yml
```

As a pre-commit hook:

```yaml
# .pre-commit-config.yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/python
    rev: ""
    hooks:
      - id: python-import-linter
```

### `python-isort`

Sort Python module imports with [isort](https://github.com/PyCQA/isort).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-isort.yml
```

As a pre-commit hook:

```yaml
# .pre-commit-config.yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/python
    rev: ""
    hooks:
      - id: python-isort
```

### `python-mypy`

Perform static type checking with [Mypy](https://github.com/python/mypy).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-mypy.yml
```

### `python-pip-compile`

Lock Python environments with [uv pip compile](https://docs.astral.sh/uv/pip/compile/).

As a pre-commit hook:

```yaml
# .pre-commit-config.yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/python
    rev: ""
    hooks:
      - id: python-pip-compile
```

### `python-pyroma`

Rate Python package readiness with [pyroma](https://github.com/regebro/pyroma).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-pyroma.yml
```

As a pre-commit hook:

```yaml
# .pre-commit-config.yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/python
    rev: ""
    hooks:
      - id: python-pyroma
```

### `python-pytest`

Run unit tests with [pytest](https://docs.pytest.org/en/stable/).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-pytest.yml
```

As a pre-commit hook:

```yaml
# .pre-commit-config.yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/python
    rev: ""
    hooks:
      - id: python-pytest
```

### `python-twine-upload`

Publish a Python package to PyPI with [twine](https://github.com/pypa/twine) using an API token.

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-twine-upload.yml
```

### `python-twine-upload-pypi-oidc`

Publish a Python package to PyPI with [twine](https://github.com/pypa/twine) using [OIDC](https://docs.pypi.org/trusted-publishers/).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-twine-upload-pypi-oidc.yml
```

### `python-vulture`

Find dead Python code with [vulture](https://github.com/jendrikseipp/vulture).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-vulture.yml
```

As a pre-commit hook:

```yaml
# .pre-commit-config.yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/python
    rev: ""
    hooks:
      - id: python-vulture
```

## Variables

### `PYTHON_DOCFORMATTER_EXCLUDE`

Directories and files to exclude from docformatter.

### `PYTHON_PACKAGE`

Python package directory.

_Default:_ `${CI_PROJECT_NAME}`

This pipeline assumes a single project contains a single package named the
name as the project. This assumption may not be valid in some situations,
such as:

- The package directory name and project name do not match.

- The project name contains characters other than `[a-zA-Z0-9_]`.

In these situations, the value must be set manually.

```yaml
# .gitlab-ci.yml
variables:
  PYTHON_PACKAGE: python_pipeline
```

### `PYTHON_PIP_CONFIG_FILE`

`pip` config file location.

_Default:_ `${CI_PROJECT_DIR}/.pip.conf`

Location where the generated pip config file should be written.

### `PYTHON_PYPI_DOWNLOAD_URL`

The URLs for private PyPI repositories to use. The upload URL defaults to
the GitLab PyPI Package Registry for the current project.

Supplying the download URL will override any other download configuration.

This pipeline expects the upstream PyPI repository to supporting
dependency proxying.

### `PYTHON_PYPI_GITLAB_GROUP_ID`

Pull packages from the GitLab Package Registry for this group ID. When
provided, this pipeline will be automatically configured to proxy PyPI
packages through GitLab.

### `PYTHON_PYPI_GITLAB_PROJECT_ID`

Pull packages from the GitLab Package Registry for this project ID. When
provided, this pipeline will be automatically configured to proxy PyPI
packages through GitLab.

### `PYTHON_PYPI_PASSWORD`

_Default:_ `${CI_JOB_TOKEN}`

The password to access the private PyPI repository URL. Defaults to
`$CI_JOB_TOKEN`.

### `PYTHON_PYPI_UPLOAD_URL`

_Default:_ `${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi`

The URLs for private PyPI repositories to use. The upload URL defaults to
the GitLab PyPI Package Registry for the current project.

Supplying the download URL will override any other download configuration.

This pipeline expects the upstream PyPI repository to supporting
dependency proxying.

### `PYTHON_PYPI_USERNAME`

_Default:_ `gitlab-ci-token`

The username to access the private PyPI repository URL. Defaults to
`gitlab-ci-token`.

### `PYTHON_PYROMA_MINIMUM_RATING`

_Default:_ `8`

Minimum rating below which the `pyroma` job will trigger a failure.

### `PYTHON_REQUIREMENTS`

_Default:_ `.`

Install extra packages as part of `pip install`.

### `PYTHON_VERSION`

_Default:_ `3.11`

Default Python version to use for jobs.

### `PYTHON_VULTURE_MINIMUM_CONFIDENCE`

_Default:_ `80`

The minimum confidence score required for vulture to cause a job to fail.
